define(['angular'], function() {
  var module;
  module = angular.module('app.states.project.details.stagetasks', []);
  return module.directive('stageTasks', function(StageService, ProjectService, TaskService, Constants, AppService) {
    return {
      restrict: "A",
      scope: {
        stage: "="
      },
      templateUrl: "app/states/project/details/stagetasks/stagetasks",
      link: function($scope, $element, $attrs) {
        var init, reset, resetEditingTask;
        $scope.editingTask = {
          name: ""
        };
        $scope.options = {
          accept: function(sourceNode, destNodes, destIndex) {
            return destNodes.$element.attr("type") === 'task';
          },
          dropped: function(event) {
            var data, dest, idStage, source, task;
            source = event.source;
            dest = event.dest;
            idStage = event.dest.nodesScope.$element.attr("data-stage-id");
            task = source.nodeScope.$modelValue;
            if (source.index !== dest.index || task.idStage !== idStage) {
              AppService.updatePos(task, dest.nodesScope.$modelValue);
              data = {
                id: task.id,
                pos: task.pos
              };
              if (idStage !== task.idStage) {
                data.idStage = idStage;
              }
              task.idStage = idStage;
              TaskService.updateTask(data);
            }
          },
          beforeDrop: function(event) {
            if (event.dest.nodesScope.$element.attr("type") !== 'task') {
              event.source.nodeScope.$$apply = false;
            }
          }
        };
        init = function() {
          return $scope.editingTask = {
            name: ""
          };
        };
        reset = function() {
          return resetEditingTask();
        };
        resetEditingTask = function() {
          return $scope.editingTask = {
            name: ""
          };
        };
        $scope.saveEditingTask = function(task) {
          var _ref;
          if (!$scope.editingTask.name) {
            return;
          }
          if (((_ref = $scope.editingTask) != null ? _ref.id : void 0) !== (task != null ? task.id : void 0)) {
            return;
          }
          return TaskService.updateTask($scope.editingTask).then(resetEditingTask);
        };
        $scope.editTask = function(task, key) {
          reset();
          $scope.editingTask = {
            id: task.id
          };
          return $scope.editingTask[key] = task[key];
        };
        $scope.destroyTask = function(task) {
          return TaskService.destroyTask(task);
        };
        $scope.resetEditingTask = resetEditingTask;
        return init();
      }
    };
  });
});

//# sourceMappingURL=stagetasks.js.map
